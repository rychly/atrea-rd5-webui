<?xml version="1.0" encoding="UTF-8"?>
<!-- see http://www.w3.org/2003/entities/2007/w3centities-f.ent" -->
<!DOCTYPE xsl:stylesheet [
        <!ENTITY nbsp  "&#x000A0;" ><!--NO-BREAK SPACE -->
        <!ENTITY ndash "&#x02013;" ><!--EN DASH -->
        <!ENTITY mdash "&#x02014;" ><!--EM DASH -->
        <!ENTITY deg   "&#x000B0;" ><!--DEGREE SIGN -->
        <!ENTITY sup3  "&#x000B3;" ><!--SUPERSCRIPT THREE -->
        ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" doctype-system="about:legacy-compat"/>

    <xsl:variable name="title" select="'Stav VZT jednotky'"/>

    <xsl:template match="/RD5WEB/RD5">
        <!-- SYSTEM -->
        <xsl:variable name="SW_DIGI">
            <xsl:value-of select="INTEGER_R/O[@I='I00000']/@V"/>
            <xsl:text>.</xsl:text>
            <xsl:value-of select="INTEGER_R/O[@I='I00001']/@V"/>
        </xsl:variable>
        <xsl:variable name="SW_RD5">
            <xsl:value-of select="INTEGER_R/O[@I='I00020']/@V"/>
            <xsl:text>.</xsl:text>
            <xsl:value-of select="INTEGER_R/O[@I='I00021']/@V"/>
            <xsl:text>.</xsl:text>
            <xsl:value-of select="INTEGER_R/O[@I='I00022']/@V"/>
        </xsl:variable>
        <xsl:variable name="DATE">
            <xsl:value-of select="INTEGER_R/O[@I='I00006']/@V"/>
            <xsl:text>.</xsl:text>
            <xsl:value-of select="INTEGER_R/O[@I='I00005']/@V"/>
            <xsl:text>.</xsl:text>
            <xsl:value-of select="INTEGER_R/O[@I='I00004']/@V"/>
        </xsl:variable>
        <xsl:variable name="TIME">
            <xsl:value-of select="INTEGER_R/O[@I='I00007']/@V"/>
            <xsl:text>:</xsl:text>
            <xsl:value-of select="format-number(INTEGER_R/O[@I='I00008']/@V, '00')"/>
            <xsl:text>:</xsl:text>
            <xsl:value-of select="format-number(INTEGER_R/O[@I='I00009']/@V, '00')"/>
        </xsl:variable>
        <!-- TEMP -->
        <xsl:variable name="ETA"><!-- T-ETA -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I10213']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10213']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I10213']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10213']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="SUP"><!-- T-SUP -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I10212']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10212']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I10212']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10212']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ODA"><!-- T-ODA -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I10211']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10211']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I10211']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10211']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="EHA"><!-- T-EHA -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I10214']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10214']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I10214']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10214']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="IDA"><!-- T-IDA -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I10215']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10215']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I10215']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I10215']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ODA_P"><!-- T-ODA průměrná -->
            <xsl:choose>
                <xsl:when test="INTEGER_R/O[@I='I11420']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I11420']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_R/O[@I='I11420']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_R/O[@I='I11420']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ODA_P_INT"><!-- T-ODA průměrovací interval -->
            <xsl:choose>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=0">1&nbsp;hod.</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=1">3&nbsp;hod.</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=2">6&nbsp;hod.</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=3">12&nbsp;hod.</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=4">1&nbsp;den</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=5">2&nbsp;dny</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=6">3&nbsp;dny</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=7">4&nbsp;dny</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=8">5&nbsp;dnů</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=9">6&nbsp;dnů</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=10">7&nbsp;dnů</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=11">8&nbsp;dnů</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=12">9&nbsp;dnů</xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11431']/@V=13">10&nbsp;dnů</xsl:when>
                <xsl:otherwise>???</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="NETOP_SEZ"
                      select="INTEGER_R/O[@I='I11401']/@V"/><!-- Aktuální sezóna (0=Topná, 1=Netopná) -->
        <xsl:variable name="NETOP_SEZ_ODA_P"><!-- T-ODA průměrná pro přepnutí sezóny (pod=Topná, nad=Netopná); pokud H11401=2 -->
            <xsl:choose>
                <xsl:when test="INTEGER_RW/O[@I='H11402']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11402']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11402']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11402']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="NI" select="($SUP - $ODA) div ($ETA - $ODA)"/><!-- ucinnost interier -->
        <xsl:variable name="NE" select="($EHA - $ETA) div ($ODA - $ETA)"/><!-- ucinnost exterier -->
        <!-- VENT -->
        <!-- H10510=1 (Konstantní průtok), Y v m3/h, X v %: Y = $MIN_M3 + ( ($MAX_M3 - $MIN_M3) div (99 * (X - 1)) ) -->
        <xsl:variable name="SUP_P" select="INTEGER_R/O[@I='I11600']/@V"/><!-- SUP požadavek -->
        <xsl:variable name="SUP_A" select="INTEGER_R/O[@I='I11602']/@V"/><!-- SUP aktuální průtok -->
        <xsl:variable name="SUP_M"
                      select="INTEGER_RW/O[@I='H10201']/@V div 1000"/><!-- Výstup M2/RD5int: M-SUP ovládání otáček ventilátoru M2 signálem 0-10V-->
        <xsl:variable name="ETA_P" select="INTEGER_R/O[@I='I11601']/@V"/><!-- ETA požadavek -->
        <xsl:variable name="ETA_A" select="INTEGER_R/O[@I='I11603']/@V"/><!-- ETA aktuální průtok -->
        <xsl:variable name="ETA_M"
                      select="INTEGER_RW/O[@I='H10200']/@V div 1000"/><!-- Výstup M1/RD5int: M-ETA ovládání otáček ventilátoru M1 signálem 0-10V -->
        <xsl:variable name="AUTO_V"
                      select="INTEGER_RW/O[@I='H10502']/@V"/><!-- Interval větrání v režim Automat v každé hodině provozu -->
        <xsl:variable name="MIN_M3" select="INTEGER_R/O[@I='I12003']/@V"/><!-- Nejnižší výkon jednotky v m3/h -->
        <xsl:variable name="MAX_M3" select="INTEGER_R/O[@I='I12002']/@V"/><!-- Nejvyšší výkon jednotky v m3/h -->
        <xsl:variable name="VENT_MIN"
                      select="INTEGER_RW/O[@I='H11616']/@V"/><!-- Omezení minima, tj. nejnižší výkon v uživatelském přístupu v % -->
        <xsl:variable name="VENT_MAX"
                      select="INTEGER_RW/O[@I='H11617']/@V"/><!-- Omezení maxima, tj. nejvyšší výkon v uživatelském přístupu v % -->
        <xsl:variable name="VENT_MIN_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($VENT_MIN - 1))"/><!-- Omezení minima, tj. nejnižší výkon v uživatelském přístupu v m3/h -->
        <xsl:variable name="VENT_MAX_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($VENT_MAX - 1))"/><!-- Omezení maxima, tj. nejvyšší výkon v uživatelském přístupu v m3/h -->
        <!-- AUX -->
        <xsl:variable name="PRED"
                      select="INTEGER_RW/O[@I='H10202']/@V div 1000"/><!-- Výstupu SA1: ovládání předehřevu signálem digitálním 0/10V (PWM regulace) -->
        <xsl:variable name="PRED_EHA"><!-- Teploty odváděného vzduchu po rekuperaci (EHA), při které se spustí předehřev -->
            <xsl:choose>
                <xsl:when test="INTEGER_RW/O[@I='H11501']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11501']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11501']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11501']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="PRED_ODA"><!-- Požadovaná teplota, na kterou má předehřívač ohřát venkovní vzduch nasávaný do jednotky (ODA) -->
            <xsl:choose>
                <xsl:when test="INTEGER_RW/O[@I='H11502']/@V &lt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11502']/@V) div 10"/>
                </xsl:when>
                <xsl:when test="INTEGER_RW/O[@I='H11502']/@V &gt; (65536 div 2)">
                    <xsl:value-of select="(INTEGER_RW/O[@I='H11502']/@V - 65536) div 10"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="BYPASS_O"
                      select="INTEGER_RW/O[@I='H10205']/@V div 100"/><!-- Časový interval sepnutí výstupu SB plus (sekund): otevírání klapky bypassu na dobu 1x v rámci nastavené periody -->
        <xsl:variable name="BYPASS_C"
                      select="INTEGER_RW/O[@I='H10206']/@V div 100"/><!-- Časový interval sepnutí výstupu SB plus (sekund): zavírání klapky bypassu na dobu 1x v rámci nastavené periody -->
        <xsl:variable name="BYPASS_P"
                      select="INTEGER_RW/O[@I='H11803']/@V"/><!-- Délka periody provádění regulačního zásahu klapky bypassu -->
        <xsl:variable name="BYPASS_N"
                      select="INTEGER_RW/O[@I='H11804']/@V"/><!-- Násobitel periody provádění regulačního zásahu klapky bypassu -->
        <xsl:variable name="BYPASS_DELTA_T"
                      select="INTEGER_RW/O[@I='H11812']/@V div 10"/><!--  Teplotní odstup teploty měřené od požadované pro bypass (při větším rozdílu v TNS pustí teplejší a v NTS chladnější vzduch přímo) -->
        <!-- EXT -->
        <xsl:variable name="D1" select="DIGITAL_R/O[@I='D10200']/@V"/><!-- Stav vstupu D1, přivedeno napětí 230V -->
        <xsl:variable name="D1_P"
                      select="INTEGER_RW/O[@I='H10603']/@V"/><!-- Výkon větrací jednotky při sepnutí vstupu D1 nastavitelný v % -->
        <xsl:variable name="D1_P_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($D1_P - 1))"/><!-- Výkon větrací jednotky při sepnutí vstupu D1 nastavitelný v m3/h -->
        <xsl:variable name="D1_START"
                      select="INTEGER_RW/O[@I='H10605']/@V"/><!--  Délka zpoždění spuštění větrání po sepnutí vstupu D1 -->
        <xsl:variable name="D1_STOP"
                      select="INTEGER_RW/O[@I='H10606']/@V"/><!-- Délka zpožděného vypnutí větrání po vypnutí vstupu D1 -->
        <xsl:variable name="DX_MAX"
                      select="INTEGER_RW/O[@I='H10607']/@V"/><!-- Nejdelší doba běhu větrání dle vstupů D1-D4 od aktivace vstupů -->
        <!-- MODE: 1=Program, 2=Manual -->
        <xsl:variable name="MODE_OPER" select="INTEGER_RW/O[@I='H10701']/@V"/><!-- Mód nastavení režimu -->
        <xsl:variable name="MODE_VENT" select="INTEGER_RW/O[@I='H10700']/@V"/><!-- Mód nastavení výkonu -->
        <xsl:variable name="MODE_TEMP" select="INTEGER_RW/O[@I='H10702']/@V"/><!-- Mód nastavení teploty -->
        <xsl:variable name="MODE_ZONE" select="INTEGER_RW/O[@I='H10703']/@V"/><!-- Mód nastavení zóny -->
        <xsl:variable name="MODE_FORCE" select="INTEGER_RW/O[@I='H10712']/@V"/><!-- Vnucený stav (3=D1) -->
        <!-- OPER: 0=Vypnute, 1=Automat, 2=Ventilace, 5=Bypass, 6=Rozvazeni -->
        <xsl:variable name="OPER_REQ" select="INTEGER_RW/O[@I='H10705']/@V"/><!-- Požadovaný režim -->
        <xsl:variable name="OPER_MAN" select="INTEGER_RW/O[@I='H10709']/@V"/><!-- Poslední ručně zadaný režim -->
        <xsl:variable name="OPER_AKT" select="INTEGER_RW/O[@I='H10715']/@V"/><!-- Aktuální požadavaný režim -->
        <!-- CONTROL -->
        <xsl:variable name="VENT_REQ" select="INTEGER_RW/O[@I='H10704']/@V"/><!-- Požadovaný výkon v % -->
        <xsl:variable name="VENT_MAN" select="INTEGER_RW/O[@I='H10708']/@V"/><!-- Poslední ručně zadaný výkon v % -->
        <xsl:variable name="VENT_AKT" select="INTEGER_RW/O[@I='H10714']/@V"/><!-- Výkon - aktuální požadavek v % -->
        <xsl:variable name="VENT_REQ_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($VENT_REQ - 1))"/><!-- Požadovaný výkon v m3/h při H10510=1 (Konstantní průtok) -->
        <xsl:variable name="VENT_MAN_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($VENT_MAN - 1))"/><!-- Poslední ručně zadaný výkon v m3/h při H10510=1 (Konstantní průtok) -->
        <xsl:variable name="VENT_AKT_M3"
                      select="$MIN_M3 + (($MAX_M3 - $MIN_M3) div 99 * ($VENT_AKT - 1))"/><!-- Výkon - aktuální požadavek v m3/h při H10510=1 (Konstantní průtok) -->
        <xsl:variable name="TEMP_REQ" select="INTEGER_RW/O[@I='H10706']/@V div 10"/><!-- Požadovaná teplota -->
        <xsl:variable name="TEMP_MAN"
                      select="INTEGER_RW/O[@I='H10710']/@V div 10"/><!-- Poslední ručně zadaná teplota -->
        <xsl:variable name="TEMP_AKT"
                      select="INTEGER_RW/O[@I='H10716']/@V div 10"/><!-- Teplota - aktuální požadavek -->
        <xsl:variable name="ZONE_REQ" select="INTEGER_RW/O[@I='H10707']/@V"/><!-- Požadovaná zóna -->
        <xsl:variable name="ZONE_MAN" select="INTEGER_RW/O[@I='H10711']/@V"/><!-- Poslední ručně zadaná zóna -->
        <xsl:variable name="ZONE_AKT" select="INTEGER_RW/O[@I='H10717']/@V"/><!-- Zóna - aktuální požadavek -->
        <!-- ALARM -->
        <xsl:variable name="ALARM_OVERHEAT" select="DIGITAL_R/O[@I='D11100']/@V"/><!-- Přehřátí jednotky -->
        <xsl:variable name="ALARM_UNSET_ORIENTATION" select="DIGITAL_R/O[@I='D11101']/@V"/><!-- Nenastavena orientace -->
        <xsl:variable name="ALARM_UNSET_HEATER" select="DIGITAL_R/O[@I='D11102']/@V"/><!-- Nenastaven typ ohřívače -->
        <xsl:variable name="ALARM_FROST2" select="DIGITAL_R/O[@I='D11103']/@V"/><!-- 2. mrazová ochrana -->
        <xsl:variable name="ALARM_STP_ACTIVE" select="DIGITAL_R/O[@I='D11104']/@V"/><!-- STP kontakt aktivní -->
        <xsl:variable name="ALARM_FAIL_DP1" select="DIGITAL_R/O[@I='D11105']/@V"/><!-- Porucha manometru DP1 -->
        <xsl:variable name="ALARM_FAIL_DP2" select="DIGITAL_R/O[@I='D11106']/@V"/><!-- Porucha manometru DP2 -->
        <xsl:variable name="ALARM_FAIL_TU1" select="DIGITAL_R/O[@I='D11107']/@V"/><!-- Porucha čidla TU1 -->
        <xsl:variable name="ALARM_FAIL_TU2" select="DIGITAL_R/O[@I='D11108']/@V"/><!-- Porucha čidla TU2 -->
        <xsl:variable name="ALARM_FAIL_TEA" select="DIGITAL_R/O[@I='D11109']/@V"/><!-- Porucha čidla TEa -->
        <xsl:variable name="ALARM_FAIL_TEB" select="DIGITAL_R/O[@I='D11110']/@V"/><!-- Porucha čidla TEb -->
        <xsl:variable name="ALARM_FAIL_TA2" select="DIGITAL_R/O[@I='D11111']/@V"/><!-- Porucha čidla TA2 -->
        <xsl:variable name="ALARM_FAIL_RDIO" select="DIGITAL_R/O[@I='D11112']/@V"/><!-- Porucha komunikace s RD-IO -->
        <xsl:variable name="ALARM_FAIL_RD4_FLOW" select="DIGITAL_R/O[@I='D11113']/@V"/><!-- Porucha kom. s RD4-flow -->
        <xsl:variable name="ALARM_UNBALANCED" select="DIGITAL_R/O[@I='D11114']/@V"/><!-- Nevyrovnaný průtok -->
        <xsl:variable name="ALARM_FROST1" select="DIGITAL_R/O[@I='D11115']/@V"/><!-- 1. mrazová ochrana -->
        <xsl:variable name="ALARM_FAIL_SPACE" select="DIGITAL_R/O[@I='D11116']/@V"/><!-- Porucha čidla v prostoru -->
        <xsl:variable name="ALARM_FROZEN" select="DIGITAL_R/O[@I='D11117']/@V"/><!-- Zámraz rekuperátoru -->
        <xsl:variable name="ALARM_UNFREEZING" select="DIGITAL_R/O[@I='D11118']/@V"/><!-- Odmražování rekuperátoru -->
        <xsl:variable name="ALARM_EXPENSIVE" select="DIGITAL_R/O[@I='D11119']/@V"/><!-- Vysoký tarif -->
        <xsl:variable name="ALARM_SLOW" select="DIGITAL_R/O[@I='D11120']/@V"/><!-- Nedostatečný průtok -->
        <xsl:variable name="ALARM_WEAK_HEATER" select="DIGITAL_R/O[@I='D11121']/@V"/><!-- Nedostatečný výkon 1.topení -->
        <xsl:variable name="ALARM_FILTER" select="DIGITAL_R/O[@I='D11122']/@V"/><!-- Zanesený filtr -->
        <xsl:variable name="ALARM_FAIL_IN1" select="DIGITAL_R/O[@I='D11123']/@V"/><!-- Porucha AI vstupu IN1 -->
        <xsl:variable name="ALARM_FAIL_IN2" select="DIGITAL_R/O[@I='D11124']/@V"/><!-- Porucha AI vstupu IN2 -->
        <xsl:variable name="ALARM_FAIL_INK11" select="DIGITAL_R/O[@I='D11125']/@V"/><!-- Porucha AI vstupu INk1/1 -->
        <xsl:variable name="ALARM_FAIL_INK21" select="DIGITAL_R/O[@I='D11126']/@V"/><!-- Porucha AI vstupu INk2/1 -->
        <xsl:variable name="ALARM_FAIL_INK31" select="DIGITAL_R/O[@I='D11127']/@V"/><!-- Porucha AI vstupu INk3/1 -->
        <xsl:variable name="ALARM_FAIL_INK41" select="DIGITAL_R/O[@I='D11128']/@V"/><!-- Porucha AI vstupu INk4/1 -->
        <xsl:variable name="ALARM_FAIL_INK12" select="DIGITAL_R/O[@I='D11129']/@V"/><!-- Porucha AI vstupu INk1/2 -->
        <xsl:variable name="ALARM_FAIL_INK22" select="DIGITAL_R/O[@I='D11130']/@V"/><!-- Porucha AI vstupu INk2/2 -->
        <xsl:variable name="ALARM_FAIL_INK32" select="DIGITAL_R/O[@I='D11131']/@V"/><!-- Porucha AI vstupu INk3/2 -->
        <xsl:variable name="ALARM_FAIL_INK42" select="DIGITAL_R/O[@I='D11132']/@V"/><!-- Porucha AI vstupu INk4/2 -->
        <xsl:variable name="ALARM_FAIL_INTERNAL" select="DIGITAL_R/O[@I='D11136']/@V"/><!-- interní použití (porucha T-ODA) -->
        <xsl:variable name="ALARM_UNSET_UNIT" select="DIGITAL_R/O[@I='D11140']/@V"/><!-- Jednotka není zprovozněna -->
        <xsl:variable name="ALARM_UNSET_CFG" select="DIGITAL_R/O[@I='D11141']/@V"/><!-- Chybí konfigurační soubor -->
        <xsl:variable name="ALARM_ALT_CFG" select="DIGITAL_R/O[@I='D11142']/@V"/><!-- Alternativní konfigurační soubor -->
        <xsl:variable name="ALARM_WEAK_PREHEATER" select="DIGITAL_R/O[@I='D11143']/@V"/><!-- Nedostatečné předehřátí -->
        <xsl:variable name="ALARM_INCOMPAT_HEATERS" select="DIGITAL_R/O[@I='D11144']/@V"/><!-- Nepovolená kombinace ohřívačů -->
        <xsl:variable name="ALARM_FAIL_DP3" select="DIGITAL_R/O[@I='D11145']/@V"/><!-- Porucha manometru DP3 -->
        <xsl:variable name="ALARM_FAIL_DPEXT" select="DIGITAL_R/O[@I='D11146']/@V"/><!-- Porucha externího manometru -->
        <xsl:variable name="ALARM_FAIL_RD5_K1" select="DIGITAL_R/O[@I='D11147']/@V"/><!-- Porucha modulu RD5-K/1 -->
        <xsl:variable name="ALARM_FAIL_RD5_K2" select="DIGITAL_R/O[@I='D11148']/@V"/><!-- Porucha modulu RD5-K/2 -->
        <xsl:variable name="ALARM_UNFREEZING_HEATPUMP" select="DIGITAL_R/O[@I='D11149']/@V"/><!-- Odmrazování TČ -->
        <xsl:variable name="ALARM_FAIL_TRK11" select="DIGITAL_R/O[@I='D11150']/@V"/><!-- Porucha čidla TRk1 - Modul 1 -->
        <xsl:variable name="ALARM_FAIL_TRK21" select="DIGITAL_R/O[@I='D11151']/@V"/><!-- Porucha čidla TRk2 - Modul 1 -->
        <xsl:variable name="ALARM_FAIL_TRK31" select="DIGITAL_R/O[@I='D11152']/@V"/><!-- Porucha čidla TRk3 - Modul 1 -->
        <xsl:variable name="ALARM_FAIL_TRK41" select="DIGITAL_R/O[@I='D11153']/@V"/><!-- Porucha čidla TRk4 - Modul 1 -->
        <xsl:variable name="ALARM_FAIL_TRK51" select="DIGITAL_R/O[@I='D11154']/@V"/><!-- Porucha čidla TRk5 - Modul 1 -->
        <xsl:variable name="ALARM_FAIL_TRK12" select="DIGITAL_R/O[@I='D11155']/@V"/><!-- Porucha čidla TRk1 - Modul 2 -->
        <xsl:variable name="ALARM_FAIL_TRK22" select="DIGITAL_R/O[@I='D11156']/@V"/><!-- Porucha čidla TRk2 - Modul 2 -->
        <xsl:variable name="ALARM_FAIL_TRK32" select="DIGITAL_R/O[@I='D11157']/@V"/><!-- Porucha čidla TRk3 - Modul 2 -->
        <xsl:variable name="ALARM_FAIL_TRK42" select="DIGITAL_R/O[@I='D11158']/@V"/><!-- Porucha čidla TRk4 - Modul 2 -->
        <xsl:variable name="ALARM_FAIL_TRK52" select="DIGITAL_R/O[@I='D11159']/@V"/><!-- Porucha čidla TRk5 - Modul 2 -->
        <xsl:variable name="ALARM_UPDATING" select="DIGITAL_R/O[@I='D11160']/@V"/><!-- Probíhá update -->
        <xsl:variable name="ALARM_FAIL_UPDATE" select="DIGITAL_R/O[@I='D11161']/@V"/><!-- Update skončil s chybou -->
        <xsl:variable name="ALARM_INCOMPAT_COOLERS" select="DIGITAL_R/O[@I='D11162']/@V"/><!-- Nepovolená konfigurace chlazení -->
        <xsl:variable name="ALARM_TRIAL_ACTIVE" select="DIGITAL_R/O[@I='D11163']/@V"/><!-- Zkušební provoz aktivní -->
        <xsl:variable name="ALARM_TRIAL_DONE" select="DIGITAL_R/O[@I='D11164']/@V"/><!-- Zkušební provoz vypršel -->
        <xsl:variable name="ALARM_EMERGENCY" select="DIGITAL_R/O[@I='D11165']/@V"/><!-- Nouzový režim aktivní -->
        <xsl:variable name="ALARM_FROST_PROTECTION" select="DIGITAL_R/O[@I='D11166']/@V"/><!-- Mrazová ochrana-kapilára -->
        <xsl:variable name="ALARM_FROST_CFG" select="DIGITAL_R/O[@I='D11167']/@V"/><!-- Konfigurace mrazové ochrany -->
        <xsl:variable name="ALARM_UNLEARNED" select="DIGITAL_R/O[@I='D11168']/@V"/><!-- Nedokončené učení jednotky -->
        <xsl:variable name="ALARM_FAIL_TODA" select="DIGITAL_R/O[@I='D11169']/@V"/><!-- Porucha čidla T-ODA -->
        <xsl:variable name="ALARM_FAIL_TETA_TIDA" select="DIGITAL_R/O[@I='D11170']/@V"/><!-- Porucha čidla T-ETA/T-IDA -->
        <xsl:variable name="ALARM_NO_COOLING" select="DIGITAL_R/O[@I='D11171']/@V"/><!-- Chlazení není dostupné -->
        <xsl:variable name="ALARM_NO_HEATING" select="DIGITAL_R/O[@I='D11172']/@V"/><!-- Ohřev není k dispozici -->
        <xsl:variable name="ALARM_EVAP" select="DIGITAL_R/O[@I='D11173']/@V"/><!-- Odpaření aktivní -->
        <xsl:variable name="ALARM_TURBO_HEATING" select="DIGITAL_R/O[@I='D11174']/@V"/><!-- Zvýšený výkon při zátopu -->
        <xsl:variable name="ALARM_BAD_FLOW_SETTING" select="DIGITAL_R/O[@I='D11175']/@V"/><!-- Nepovolené nastavení průtoků -->
        <xsl:variable name="ALARM_FILTER_PERIOD" select="DIGITAL_R/O[@I='D11183']/@V"/><!-- Interval výměny filtru -->
        <xsl:variable name="ALARM_FILTER_TEST" select="DIGITAL_R/O[@I='D11184']/@V"/><!-- Konfigurace testu filtru -->
        <xsl:variable name="ALARM_BLOCKED" select="DIGITAL_R/O[@I='D11185']/@V"/><!-- Blokace chodu -->
        <!-- -->
        <html xmlns="http://www.w3.org/1999/xhtml" lang="cs" xml:lang="cs">
            <head>
                <title>
                    <xsl:value-of select="$title"/>
                </title>
                <meta http-equiv="refresh" content="10"/>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="$title"/>
                </h1>
                <table>
                    <tr>
                        <td colspan="2"/>
                        <th>Interiér</th>
                        <td/>
                    </tr>
                    <tr>
                        <th>Místnosti</th>
                        <td>
                            <xsl:value-of select="concat(format-number($IDA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <progress max="{$IDA}" value="{$IDA}"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <th>Odtah</th>
                        <td>
                            <xsl:value-of select="concat(format-number($ETA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <progress max="{$IDA}" value="{$ETA}"/>
                        </td>
                        <td>
                            <xsl:value-of select="concat(format-number($ETA - $IDA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Přívod</th>
                        <td>
                            <xsl:value-of select="concat(format-number($SUP, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <progress max="{$IDA}" value="{$SUP}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($SUP - $IDA, '0.0'), '&nbsp;&deg;C, ', format-number($SUP - $ETA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Účinnost</th>
                        <td>
                            <xsl:value-of select="format-number($NI, '0.0%')"/>
                        </td>
                        <td>
                            <progress max="1" value="{$NI}"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <th>Zvenku</th>
                        <td>
                            <xsl:value-of select="concat(format-number($SUP_A, '0'), '&nbsp;m&sup3;/h')"/>
                        </td>
                        <td>
                            <progress max="10000" value="{$SUP_M * 1000}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($SUP_P, '0'), '&nbsp;m&sup3;/h, ', format-number($SUP_M, '0.000'), '&nbsp;V')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Bypass</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$BYPASS_O&gt;0">OTEV</xsl:when>
                                <xsl:when test="$BYPASS_C&gt;0">UZAV</xsl:when>
                                <xsl:otherwise>-</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <progress max="70" value="{$BYPASS_O}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat('otev.&nbsp;', $BYPASS_O, ', uzav.&nbsp;', $BYPASS_C, ', ', $BYPASS_N, 'x za ', $BYPASS_P, '&nbsp;sek., při rozdílu &gt;', format-number($BYPASS_DELTA_T, '0.0'), '&nbsp;&deg;C ')"/>
                            <xsl:choose>
                                <xsl:when test="$NETOP_SEZ=0">topí</xsl:when>
                                <xsl:when test="$NETOP_SEZ=1">chladí</xsl:when>
                            </xsl:choose>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"/>
                        <th>Exteriér</th>
                        <td/>
                    </tr>
                    <tr>
                        <th>Sání</th>
                        <td>
                            <xsl:value-of select="concat(format-number($ODA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <progress max="{$EHA}" value="{$ODA}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($ODA - $EHA, '0.0'), '&nbsp;&deg;C, ~&nbsp;', format-number($ODA_P, '0.0'), '&nbsp;&deg;C za ', $ODA_P_INT)"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Výfuk</th>
                        <td>
                            <xsl:value-of select="concat(format-number($EHA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <progress max="{$EHA}" value="{$EHA}"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <th>Účinnost</th>
                        <td>
                            <xsl:value-of select="format-number($NE, '0.0%')"/>
                        </td>
                        <td>
                            <progress max="1" value="{$NE}"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <th>Zevnitř</th>
                        <td>
                            <xsl:value-of select="concat(format-number($ETA_A, '0'), '&nbsp;m&sup3;/h')"/>
                        </td>
                        <td>
                            <progress max="10000" value="{$ETA_M * 1000}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($ETA_P, '0'), '&nbsp;m&sup3;/h, ', format-number($ETA_M, '0.000'), '&nbsp;V')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Předehřev</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$PRED&gt;0">ANO</xsl:when>
                                <xsl:otherwise>NE</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <progress max="10000" value="{$PRED * 1000}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($PRED, '0.000'), '&nbsp;V, pod ', format-number($PRED_EHA, '0.0'), '&nbsp;&deg;C ohřát na ', format-number($PRED_ODA, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"/>
                        <th>Systém</th>
                        <td/>
                    </tr>
                    <tr>
                        <th>Datum</th>
                        <td/>
                        <td>
                            <xsl:value-of select="concat($DATE, ', ', $TIME)"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <th>Sezóna</th>
                        <td>
                            <xsl:value-of select="concat(format-number($ODA_P, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$NETOP_SEZ=0">topná</xsl:when>
                                <xsl:when test="$NETOP_SEZ=1">NEtopná</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat('průměr za ', $ODA_P_INT, ', hranice ', format-number($NETOP_SEZ_ODA_P, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Regulace</th>
                        <td>
                            <xsl:value-of select="concat($VENT_MIN, '&ndash;', $VENT_MAX, '%')"/>
                        </td>
                        <td>
                            <xsl:value-of select="concat('z ', $MIN_M3, '&ndash;', $MAX_M3, '&nbsp;m&sup3;/h')"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat('tj. ', format-number($VENT_MIN_M3, '0'), '&ndash;', format-number($VENT_MAX_M3, '0'), '&nbsp;m&sup3;/h')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Software</th>
                        <td/>
                        <td>
                            <xsl:value-of select="concat('RD5&nbsp;', $SW_RD5, ', DIGI&nbsp;', $SW_DIGI)"/>
                        </td>
                        <td/>
                    </tr>
                    <tr>
                        <td colspan="2"/>
                        <th>Řízení</th>
                        <td/>
                    </tr>
                    <tr>
                        <th>Nárazové</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$D1&gt;0">ANO</xsl:when>
                                <xsl:when test="$MODE_FORCE&gt;0">AKTIVNÍ</xsl:when>
                                <xsl:otherwise>NE</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <progress max="${VENT_MAX}" value="{$D1_P * $D1}"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat(format-number($D1_P_M3, '0'), '&nbsp;m&sup3;/h za ', $D1_START, '&nbsp;sek. po dobu ', $D1_STOP, '&nbsp;sek., max. ', $DX_MAX, '&nbsp;sek.')"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Režim</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$MODE_OPER=1">PROG</xsl:when>
                                <xsl:when test="$MODE_OPER=2">MAN</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$OPER_AKT=0">vypnuto</xsl:when>
                                <xsl:when test="$OPER_AKT=1">automat</xsl:when>
                                <xsl:when test="$OPER_AKT=2">větrání</xsl:when>
                                <xsl:when test="$OPER_AKT=5">bypass</xsl:when>
                                <xsl:when test="$OPER_AKT=6">rozvážení</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            požadavek:
                            <xsl:choose>
                                <xsl:when test="$OPER_REQ=0">vypnuto</xsl:when>
                                <xsl:when test="$OPER_REQ=1">automat</xsl:when>
                                <xsl:when test="$OPER_REQ=2">větrání</xsl:when>
                                <xsl:when test="$OPER_REQ=5">bypass</xsl:when>
                                <xsl:when test="$OPER_REQ=6">rozvážení</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                            | poslední ručně:
                            <xsl:choose>
                                <xsl:when test="$OPER_MAN=0">vypnuto</xsl:when>
                                <xsl:when test="$OPER_MAN=1">automat</xsl:when>
                                <xsl:when test="$OPER_MAN=2">větrání</xsl:when>
                                <xsl:when test="$OPER_MAN=5">bypass</xsl:when>
                                <xsl:when test="$OPER_MAN=6">rozvážení</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                    <tr>
                        <th/>
                        <td/>
                        <td colspan="2">
                            <form method="post">
                                <button name="oper" value="m1o">program</button>
                                <button name="oper" value="m2o0">vypnuto</button>
                                <button name="oper" value="m2o1">automat</button>
                                <button name="oper" value="m2o2">větrání</button>
                                <button name="oper" value="m2o5">bypass</button>
                                <button name="oper" value="m2o6">rozvážení</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Větrání</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$MODE_VENT=1">PROG</xsl:when>
                                <xsl:when test="$MODE_VENT=2">MAN</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <input type="range" min="{$VENT_MIN}" max="{$VENT_MAX}" value="{$VENT_AKT}"
                                   oninput="ventSlider(this, document.getElementById('vent_slider'), {$MIN_M3}, {$MAX_M3})"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat('požadavek: ', format-number($VENT_REQ_M3, '0'), '&nbsp;m&sup3;/h | poslední ručně: ', format-number($VENT_MAN_M3, '0'), '&nbsp;m&sup3;/h')"/>
                        </td>
                    </tr>
                    <tr>
                        <th/>
                        <td>
                            <xsl:value-of select="concat(format-number($VENT_AKT_M3, '0'), '&nbsp;m&sup3;/h')"/>
                        </td>
                        <td colspan="2">
                            <form method="post">
                                <button name="vent" value="m1v">program</button>
                                <button name="vent" id="vent_slider" value="m2v{$VENT_AKT}">
                                    <xsl:value-of select="concat(format-number($VENT_AKT_M3, '0'), '&nbsp;m&sup3;/h')"/>
                                </button>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    function ventSlider(slider, affected, min, max) {
                                        affected.value = 'm2v' + slider.value;
                                        affected.innerHTML = (parseInt(min) + ((parseInt(max) - parseInt(min)) / 99 * (parseInt(slider.value) - 1))).toFixed(0) + '&nbsp;m&sup3;/h';
                                    }
                                    //]]>
                                </script>
                                /
                                <xsl:call-template name="ventButtons">
                                    <xsl:with-param name="pStart" select="$VENT_MIN"/>
                                    <xsl:with-param name="pEnd" select="$VENT_MAX"/>
                                    <xsl:with-param name="pFirst" select="$VENT_MIN"/>
                                    <xsl:with-param name="pLast" select="$VENT_MAX"/>
                                    <xsl:with-param name="pMinM3" select="$MIN_M3"/>
                                    <xsl:with-param name="pMaxM3" select="$MAX_M3"/>
                                </xsl:call-template>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Teplota</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$MODE_TEMP=1">PROG</xsl:when>
                                <xsl:when test="$MODE_TEMP=2">MAN</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <input type="range" min="100" max="400" value="{$TEMP_AKT * 10}"
                                   oninput="tempSlider(this, document.getElementById('temp_slider'))"/>
                        </td>
                        <td>
                            <xsl:value-of
                                    select="concat('požadavek: ', format-number($TEMP_REQ, '0.0'), '&nbsp;&deg;C | poslední ručně: ', format-number($TEMP_MAN, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                    </tr>
                    <tr>
                        <th/>
                        <td>
                            <xsl:value-of select="concat(format-number($TEMP_AKT, '0.0'), '&nbsp;&deg;C')"/>
                        </td>
                        <td colspan="2">
                            <form method="post">
                                <button name="temp" value="m1t">program</button>
                                <button name="temp" id="temp_slider" value="m2t{$TEMP_AKT * 10}">
                                    <xsl:value-of select="concat(format-number($TEMP_AKT, '0.0'), '&nbsp;&deg;C')"/>
                                </button>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    function tempSlider(slider, affected) {
                                        affected.value = 'm2t' + slider.value;
                                        affected.innerHTML = (parseInt(slider.value) / 10).toFixed(1) + '&nbsp;&deg;C';
                                    }
                                    //]]>
                                </script>
                                /
                                <button name="temp" value="m2t{($TEMP_AKT - 2) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT - 2, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT - 1.5) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT - 1.5, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT - 1) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT - 1, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT - 0.5) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT - 0.5, '0.0')"/>
                                </button>
                                &ndash;
                                <button name="temp" value="m2t{($TEMP_AKT + 0.5) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT + 0.5, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT + 1) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT + 1, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT + 1.5) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT + 1.5, '0.0')"/>
                                </button>
                                <button name="temp" value="m2t{($TEMP_AKT + 2) * 10}">
                                    <xsl:value-of select="format-number($TEMP_AKT + 2, '0.0')"/>
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Zóna</th>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$MODE_ZONE=1">PROG</xsl:when>
                                <xsl:when test="$MODE_ZONE=2">MAN</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="$ZONE_AKT=0">zóna 1</xsl:when>
                                <xsl:when test="$ZONE_AKT=1">zóna 2</xsl:when>
                                <xsl:when test="$ZONE_AKT=2">zóny 1+2</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            požadavek:
                            <xsl:choose>
                                <xsl:when test="$ZONE_REQ=0">zóna 1</xsl:when>
                                <xsl:when test="$ZONE_REQ=1">zóna 2</xsl:when>
                                <xsl:when test="$ZONE_REQ=2">zóny 1+2</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                            | poslední ručně:
                            <xsl:choose>
                                <xsl:when test="$ZONE_MAN=0">zóna 1</xsl:when>
                                <xsl:when test="$ZONE_MAN=1">zóna 2</xsl:when>
                                <xsl:when test="$ZONE_MAN=2">zóny 1+2</xsl:when>
                                <xsl:otherwise>???</xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                    <tr>
                        <th/>
                        <td/>
                        <td colspan="2">
                            <form method="post">
                                <button name="zone" value="m1z">program</button>
                                <button name="zone" value="m2z0">zóna 1</button>
                                <button name="zone" value="m2z1">zóna 2</button>
                                <button name="zone" value="m2z2">zóny 1+2</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Alarmy</th>
                        <td colspan="3">
                            <ul>
                                <xsl:choose>
                                    <xsl:when test="$ALARM_ALT_CFG=1"><li>Alternativní konfigurační soubor</li></xsl:when>
                                    <xsl:when test="$ALARM_BAD_FLOW_SETTING=1"><li>Nepovolené nastavení průtoků</li></xsl:when>
                                    <xsl:when test="$ALARM_BLOCKED=1"><li>Blokace chodu</li></xsl:when>
                                    <xsl:when test="$ALARM_EMERGENCY=1"><li>Nouzový režim aktivní</li></xsl:when>
                                    <xsl:when test="$ALARM_EVAP=1"><li>Odpaření aktivní</li></xsl:when>
                                    <xsl:when test="$ALARM_EXPENSIVE=1"><li>Vysoký tarif</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_DPEXT=1"><li>Porucha externího manometru</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_DP1=1"><li>Porucha manometru DP1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_DP2=1"><li>Porucha manometru DP2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_DP3=1"><li>Porucha manometru DP3</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK11=1"><li>Porucha AI vstupu INk1/1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK12=1"><li>Porucha AI vstupu INk1/2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK21=1"><li>Porucha AI vstupu INk2/1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK22=1"><li>Porucha AI vstupu INk2/2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK31=1"><li>Porucha AI vstupu INk3/1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK32=1"><li>Porucha AI vstupu INk3/2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK41=1"><li>Porucha AI vstupu INk4/1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INK42=1"><li>Porucha AI vstupu INk4/2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_INTERNAL=1"><li>interní použití (porucha T-ODA)</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_IN1=1"><li>Porucha AI vstupu IN1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_IN2=1"><li>Porucha AI vstupu IN2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_RDIO=1"><li>Porucha komunikace s RD-IO</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_RD4_FLOW=1"><li>Porucha kom. s RD4-flow</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_RD5_K1=1"><li>Porucha modulu RD5-K/1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_RD5_K2=1"><li>Porucha modulu RD5-K/2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_SPACE=1"><li>Porucha čidla v prostoru</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TA2=1"><li>Porucha čidla TA2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TEA=1"><li>Porucha čidla TEa</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TEB=1"><li>Porucha čidla TEb</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TETA_TIDA=1"><li>Porucha čidla T-ETA/T-IDA</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TODA=1"><li>Porucha čidla T-ODA</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK11=1"><li>Porucha čidla TRk1 - Modul 1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK12=1"><li>Porucha čidla TRk1 - Modul 2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK21=1"><li>Porucha čidla TRk2 - Modul 1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK22=1"><li>Porucha čidla TRk2 - Modul 2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK31=1"><li>Porucha čidla TRk3 - Modul 1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK32=1"><li>Porucha čidla TRk3 - Modul 2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK41=1"><li>Porucha čidla TRk4 - Modul 1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK42=1"><li>Porucha čidla TRk4 - Modul 2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK51=1"><li>Porucha čidla TRk5 - Modul 1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TRK52=1"><li>Porucha čidla TRk5 - Modul 2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TU1=1"><li>Porucha čidla TU1</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_TU2=1"><li>Porucha čidla TU2</li></xsl:when>
                                    <xsl:when test="$ALARM_FAIL_UPDATE=1"><li>Update skončil s chybou</li></xsl:when>
                                    <xsl:when test="$ALARM_FILTER_PERIOD=1"><li>Interval výměny filtru</li></xsl:when>
                                    <xsl:when test="$ALARM_FILTER_TEST=1"><li>Konfigurace testu filtru</li></xsl:when>
                                    <xsl:when test="$ALARM_FILTER=1"><li>Zanesený filtr</li></xsl:when>
                                    <xsl:when test="$ALARM_FROST_CFG=1"><li>Konfigurace mrazové ochrany</li></xsl:when>
                                    <xsl:when test="$ALARM_FROST_PROTECTION=1"><li>Mrazová ochrana-kapilára</li></xsl:when>
                                    <xsl:when test="$ALARM_FROST1=1"><li>1. mrazová ochrana</li></xsl:when>
                                    <xsl:when test="$ALARM_FROST2=1"><li>2. mrazová ochrana</li></xsl:when>
                                    <xsl:when test="$ALARM_FROZEN=1"><li>Zámraz rekuperátoru</li></xsl:when>
                                    <xsl:when test="$ALARM_INCOMPAT_COOLERS=1"><li>Nepovolená konfigurace chlazení</li></xsl:when>
                                    <xsl:when test="$ALARM_INCOMPAT_HEATERS=1"><li>Nepovolená kombinace ohřívačů</li></xsl:when>
                                    <xsl:when test="$ALARM_NO_COOLING=1"><li>Chlazení není dostupné</li></xsl:when>
                                    <xsl:when test="$ALARM_NO_HEATING=1"><li>Ohřev není k dispozici</li></xsl:when>
                                    <xsl:when test="$ALARM_OVERHEAT=1"><li>Přehřátí jednotky</li></xsl:when>
                                    <xsl:when test="$ALARM_SLOW=1"><li>Nedostatečný průtok</li></xsl:when>
                                    <xsl:when test="$ALARM_STP_ACTIVE=1"><li>STP kontakt aktivní</li></xsl:when>
                                    <xsl:when test="$ALARM_TRIAL_ACTIVE=1"><li>Zkušební provoz aktivní</li></xsl:when>
                                    <xsl:when test="$ALARM_TRIAL_DONE=1"><li>Zkušební provoz vypršel</li></xsl:when>
                                    <xsl:when test="$ALARM_TURBO_HEATING=1"><li>Zvýšený výkon při zátopu</li></xsl:when>
                                    <xsl:when test="$ALARM_UNBALANCED=1"><li>Nevyrovnaný průtok</li></xsl:when>
                                    <xsl:when test="$ALARM_UNFREEZING_HEATPUMP=1"><li>Odmrazování TČ</li></xsl:when>
                                    <xsl:when test="$ALARM_UNFREEZING=1"><li>Odmražování rekuperátoru</li></xsl:when>
                                    <xsl:when test="$ALARM_UNLEARNED=1"><li>Nedokončené učení jednotky</li></xsl:when>
                                    <xsl:when test="$ALARM_UNSET_CFG=1"><li>Chybí konfigurační soubor</li></xsl:when>
                                    <xsl:when test="$ALARM_UNSET_HEATER=1"><li>Nenastaven typ ohřívače</li></xsl:when>
                                    <xsl:when test="$ALARM_UNSET_ORIENTATION=1"><li>Nenastavena orientace</li></xsl:when>
                                    <xsl:when test="$ALARM_UNSET_UNIT=1"><li>Jednotka není zprovozněna</li></xsl:when>
                                    <xsl:when test="$ALARM_UPDATING=1"><li>Probíhá update</li></xsl:when>
                                    <xsl:when test="$ALARM_WEAK_HEATER=1"><li>Nedostatečný výkon 1.topení</li></xsl:when>
                                    <xsl:when test="$ALARM_WEAK_PREHEATER=1"><li>Nedostatečné předehřátí</li></xsl:when>
                                    <xsl:otherwise><li>-</li></xsl:otherwise>
                                </xsl:choose>
                            </ul>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="ventButtons">
        <xsl:param name="pStart"/>
        <xsl:param name="pEnd"/>
        <xsl:param name="pFirst"/>
        <xsl:param name="pLast"/>
        <xsl:param name="pMinM3"/>
        <xsl:param name="pMaxM3"/>
        <xsl:if test="not($pStart&gt;$pEnd)">
            <xsl:choose>
                <xsl:when test="$pStart=$pEnd">
                    <xsl:if test="($pStart = $pFirst) or ($pStart mod 5 = 0) or ($pStart = $pLast)">
                        <button name="vent" value="m2v{$pStart}">
                            <xsl:value-of
                                    select="format-number($pMinM3 + (($pMaxM3 - $pMinM3) div 99 * ($pStart - 1)), '0')"/>
                        </button>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="vMid" select="floor(($pStart + $pEnd) div 2)"/>
                    <xsl:call-template name="ventButtons">
                        <xsl:with-param name="pStart" select="$pStart"/>
                        <xsl:with-param name="pEnd" select="$vMid"/>
                        <xsl:with-param name="pFirst" select="$pFirst"/>
                        <xsl:with-param name="pLast" select="$pLast"/>
                        <xsl:with-param name="pMinM3" select="$pMinM3"/>
                        <xsl:with-param name="pMaxM3" select="$pMaxM3"/>
                    </xsl:call-template>
                    <xsl:call-template name="ventButtons">
                        <xsl:with-param name="pStart" select="$vMid + 1"/>
                        <xsl:with-param name="pEnd" select="$pEnd"/>
                        <xsl:with-param name="pFirst" select="$pFirst"/>
                        <xsl:with-param name="pLast" select="$pLast"/>
                        <xsl:with-param name="pMinM3" select="$pMinM3"/>
                        <xsl:with-param name="pMaxM3" select="$pMaxM3"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="/html[body/text()='HTTP: 403 Forbidden']">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="cs" xml:lang="cs">
            <head>
                <title>
                    <xsl:value-of select="$title"/>
                </title>
                <script type="text/javascript">
                    //<![CDATA[
                    function getCookie(n) {
                        let a = `; ${document.cookie}`.match(`;\\s*${n}=([^;]+)`);
                        return a ? a[1] : '';
                    }
                    function setCookie(name, value) {
                        document.cookie = name + "=" + escape(value) + "; path=" + location.pathname;
                    }
                    function valueFromCookie(where, name) {
                        if (where.value == '') where.value = getCookie(name);
                        return true;
                    }
                    function valueToCookie(where, elemName, cookieName) {
                        setCookie(cookieName, where.elements[elemName].value)
                        return true;
                    }
                    //]]>
                </script>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="$title"/>
                </h1>
                <form method="post" onsubmit="valueToCookie(this, 'password', 'vzt_passwd')">
                    <fieldset>
                        <legend>Zadejte heslo</legend>
                        <input type="password" name="password" autofocus="autofocus" onfocus="valueFromCookie(this, 'vzt_passwd')"/>
                        <button>Odeslat</button>
                    </fieldset>
                </form>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
