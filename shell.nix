let

  overlays = import ./overlays.nix;

in
  # Running against custom version of nixpkgs or pkgs would be as simple as running `nix-shell --arg nixpkgs /absolute/path/to/nixpkgs`
  # See https://garbas.si/2015/reproducible-development-environments.html
  { nixpkgs ? import <nixpkgs>, pkgs ? nixpkgs { inherit overlays; } }:

pkgs.mkShell rec {

  buildInputs = with pkgs; [
    uhttpd
    uci
    lua51PackagesCustom.uhttpd-lua-utils
    (with lua51PackagesCustom; lua51Packages.lua.withPackages(ps: with ps; [ luasocket md5 ]))
  ];

  shellHook = ''
    # versions
    echo "# SOFTWARE:" ${builtins.concatStringsSep ", " (map (x: x.name) buildInputs)}
  '';

}
