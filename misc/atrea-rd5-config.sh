#!/bin/sh

if [[ $# -lt 1 || "${1}" = "--help" ]]; then
	echo "Usage: ${0} [--no-comments] <device-ip-or-hostname> [session-id] [lang-number]" >&2
	echo "Connect to a Atrea RD5 device and get its current status." >&2
	echo "The language number can be: 0=Czech (default), 1=German, 2=English." >&2
	exit 1
fi

if [[ "${1}" = "--no-comments" ]]; then
	NO_COMMENTS=1
	shift
fi

IP="${1}"
SID="${2}"
LGNO="${3:-0}"

echo "Parsing or skipping texts for comments ..." >&2
[[ -z "${NO_COMMENTS}" ]] && eval `
	TEXTS_QUERY="http://${IP}/lang/texts_${LGNO}.xml"
	curl -s "${TEXTS_QUERY}" | grep -o '[IHDC][0-9][0-9][0-9][0-9][0-9]:{[^}]*}' \
	| while read TEXT; do
		KEY=${TEXT%%:*}
		# the pipeline below ends with two commands for urldecode
		VAL_T=$(echo "${TEXT}" | grep -o "t:'[^']*'" | cut -d "'" -f 2 | sed 's@+@ @g;s@%@\\\\\\x@g' | xargs -0 printf '%b' | sed 's|<br/>| |g' | tr -d '\n"')
		VAL_D=$(echo "${TEXT}" | grep -o "d:'[^']*'" | cut -d "'" -f 2 | sed 's@+@ @g;s@%@\\\\\\x@g' | xargs -0 printf '%b' | sed 's|<br/>| |g' | tr -d '\n"')
		# save to variables (also for shells without array support)
		echo TEXT_${KEY}_T=\"${VAL_T}\"
		echo TEXT_${KEY}_D=\"${VAL_D}\"
	done
`

if [[ -z "${SID}" ]]; then
	echo -n "Password: "
	read -s PASS
	MD5_PASS=`echo -n $'\r'$'\n'"${PASS}" | md5sum | cut -d ' ' -f 1`
	LOGIN_QUERY="http://${IP}/config/login.cgi?magic=${MD5_PASS}"
	SID=`curl -s "${LOGIN_QUERY}" | grep -o '>\([0-9][0-9]*\|denied\)<' | tr -d '<>'`
	if [[ -z "${SID}" || "${SID}" = "denied" ]]; then
		echo "Cannot login! A connection problem, access denied for bad password, or to many existing SIDs." >&2
		exit 1
	fi
fi

echo "Generating commented output XML document ..," >&2
XML_QUERY="http://${IP}/config/xml.xml?auth=${SID}"
curl -s "${XML_QUERY}" | sed 's/>/>\n/g' | while read LINE; do
	echo -n "${LINE}"
	if [[ -z "${NO_COMMENTS}" ]]; then
		KEY=`echo -n "${LINE}" | grep -o '[IHDC][0-9][0-9][0-9][0-9][0-9]'`
		eval VAL_T="\${TEXT_${KEY}_T}"
		eval VAL_D="\${TEXT_${KEY}_D}"
			[[ -n "${VAL_T}" ]] && echo -n "<!-- ${VAL_T} -->"
		[[ -n "${VAL_D}" ]] && echo -n "<!-- ${VAL_D} -->"
	fi
	echo
done

echo "<!-- Got a session ID! For later usage, it is recommended to reuse the session ID by running:"
echo "${0} ${IP} ${SID}"
echo "-->"
