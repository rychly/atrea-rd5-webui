local _M = {}

local uhttpd_request = require("uhttpd-request")
local http = require("socket.http")
local md5 = require("md5")
local uci = require("uci")

local cfg = uci.cursor()
local base_url = cfg:get("lua", "vzt", "config_url")
local xslt_stylesheet = '<?xml-stylesheet type="text/xsl" href="' .. cfg:get("lua", "vzt", "xslt_url")  .. '"?>'
local cookie_name = "vzt_sid"
local password_param = cfg:get("lua", "vzt", "passwd_param")

function _M.process(cmd, env)
	if (env.QUERY_STRING == "xml") then
		_M.process_xml(cmd, env)
	else
		_M.process_index(cmd, env)
	end
end

function _M.process_index(cmd, env)
	uhttpd.send("Status: 302 Found\r\n")
	uhttpd.send("Location: /lua/vzt?xml\r\n\r\n")
	return
end

function _M.login(password)
	local magic = md5.sumhexa("\r\n" .. password)
	local body, code, headers, status = http.request(base_url .. "login.cgi?magic=" .. magic)
	return body:match("<root[^>]*>%s*(%d+)%s*</root>")
end

function _M.process_xml(cmd, env)
	-- Secure cookie sent only with the https: scheme; HttpOnly cookie forbids JavaScript from accessing the cookie
	local set_cookie = "Set-Cookie: %s=%s; Secure; HttpOnly; Max-Age=%d; Path=" .. env.SCRIPT_NAME .. env.PATH_INFO
	local request_post = uhttpd_request.post_items(env)
	local cookies = uhttpd_request.cookies(env)
	local sid = (request_post[password_param] and _M.login(request_post[password_param])) or cookies[cookie_name]
	if (sid == nil) then
		uhttpd.send("Status: 200 OK\r\n")
		uhttpd.send(set_cookie:format(cookie_name, "deleted", 0) .. "\r\n")
		uhttpd.send("Content-Type: application/xml\r\n\r\n")
		uhttpd.send('<?xml version="1.0" encoding="UTF-8"?>' .. xslt_stylesheet .. '<html><body>HTTP: 403 Forbidden</body></html>')
	else
		-- CONTROL
		local ctrl_url = base_url .. "xml.cgi?auth=" .. sid
		local ctrl_params = nil
		if (request_post["oper"]) then
			local mode = request_post["oper"]:sub(1,2)
			local value = request_post["oper"]:sub(4)
			if (mode == "m1") then
				ctrl_params = "&H1070100001"
			elseif (mode == "m2" and value ~= "") then
				ctrl_params = ("&H1070100002&H10709%05d"):format(value)
			end
		elseif (request_post["vent"]) then
			local mode = request_post["vent"]:sub(1,2)
			local value = request_post["vent"]:sub(4)
			if (mode == "m1") then
				ctrl_params = "&H1070000001"
			elseif (mode == "m2" and value ~= "") then
				ctrl_params = ("&H1070000002&H10708%05d"):format(value)
			end
		elseif (request_post["temp"]) then
			local mode = request_post["temp"]:sub(1,2)
			local value = request_post["temp"]:sub(4)
			if (mode == "m1") then
				ctrl_params = "&H1070200001"
			elseif (mode == "m2" and value ~= "") then
				ctrl_params = ("&H1070200002&H10710%05d"):format(value)
			end
		elseif (request_post["zone"]) then
			local mode = request_post["zone"]:sub(1,2)
			local value = request_post["zone"]:sub(4)
			if (mode == "m1") then
				ctrl_params = "&H1070300001"
			elseif (mode == "m2" and value ~= "") then
				ctrl_params = ("&H1070300002&H10711%05d"):format(value)
			end
		end
		if (ctrl_params ~= nil) then
			local body, code, headers, status = http.request(ctrl_url .. ctrl_params)
		end
		-- STATUS
		local body, code, headers, status = http.request(base_url .. "xml.xml?auth=" .. sid)
		uhttpd.send("Status: 200 OK\r\n")
		uhttpd.send("Cache-Control: no-cache, no-store, must-revalidate\r\n")
		uhttpd.send("Pragma: no-cache\r\n")
		uhttpd.send("Expires: 0\r\n")
		uhttpd.send(set_cookie:format(cookie_name, uhttpd_request.uhttpd_urlencode(sid), 3600) .. "\r\n")
		uhttpd.send("Content-Type: application/xml\r\n\r\n")
		local xml_start, xml_end = body:find("?>", 1, true)
		if (xml_start == nil) then
			uhttpd.send('<?xml version="1.0" encoding="UTF-8"?>' .. xslt_stylesheet .. body)
		else
			uhttpd.send(body:sub(1, xml_end) .. xslt_stylesheet .. body:sub(xml_end + 1))
		end
	end
end

return _M
