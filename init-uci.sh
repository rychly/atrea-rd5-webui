#!/bin/sh

UCI_DIR=/etc/config

if ! [ -d "${UCI_DIR}" ]; then
	mkdir -p ${UCI_DIR}
	chmod 1777 ${UCI_DIR}
fi

# -- Initialize UCI config

touch ${UCI_DIR}/lua

uci -c ${UCI_DIR} set lua.vzt=lua
uci -c ${UCI_DIR} set lua.vzt.config_url='http://172.16.1.32/config/'
uci -c ${UCI_DIR} set lua.vzt.xslt_url='/vzt-xml.xslt'
uci -c ${UCI_DIR} set lua.vzt.passwd_param='password'
uci -c ${UCI_DIR} commit
