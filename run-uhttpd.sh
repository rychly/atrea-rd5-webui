#!/bin/sh

SERVER_PORT=8443
SERVER_KEY=./server-key.pem
SERVER_CRT=./server-cert.pem

[ -f "${SERVER_KEY}" -a -f "${SERVER_CRT}" ] \
|| openssl req -x509 -newkey rsa:3072 -nodes -keyout "${SERVER_KEY}" -out "${SERVER_CRT}" -days 3650 -subj '/CN=localhost'

echo "https://localhost:${SERVER_PORT}/"

exec uhttpd -f -s "${SERVER_PORT}" -C "${SERVER_CRT}" -K "${SERVER_KEY}" -h www -l /lua -L ./lua-handler.lua
