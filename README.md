# Atrea RD5 WebUI

Custom Web UI for Atrea RD5 digital control system.

## Usage

For usage, see [run-uhttpd.sh](./run-uhttpd.sh) and [lua/http-processors/vzt.lua](./lua/http-processors/vzt.lua).

### Cookies

A session cookie requires secure HTTPs connection (it wont be save in a web-browser via unsecure HTTP connection).
