-- set LUA_PATH and LUA_CPATH to load also local modules
local prefix_path = './lua';
package.path = ('%s/?.lua;%s/?/init.lua;%s'):format(prefix_path, prefix_path, package.path)
package.cpath = ('%s/?.so;%s/?/init.so;%s'):format(prefix_path, prefix_path, package.cpath)

-- Initialize UCI config
local uci = require("uci")
--uci.set_confdir("/tmp/uci")
uci.set("lua", "vzt", "lua")
uci.set("lua", "vzt", "config_url", 'http://172.16.1.32/config/')
uci.set("lua", "vzt", "xslt_url", '/vzt-xml.xslt')
uci.set("lua", "vzt", "passwd_param", 'password')
uci.commit("lua")

require("uhttpd-lua-handler")

--function handle_request(env)
--	module = require("http-processors.vzt")
--	module.process("vzt", env)
--end
